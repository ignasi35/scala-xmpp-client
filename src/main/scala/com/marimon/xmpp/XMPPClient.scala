package com.marimon.xmpp;

import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.ConnectionConfiguration
import org.jivesoftware.smack.XMPPConnection
import scala.util.Random
import org.jivesoftware.smack.XMPPException

class XMPPClient(server: String, port: Int) {

  val conn = new XMPPConnection(XMPPClient.getConfig(server, port));

  def connect(): XMPPClient = {
    conn.connect()
    return this
  }

  def disconnect(): XMPPClient = {
    if (conn.isConnected())
      conn.disconnect()
    return this
  }

  def login(username: String, password: String): XMPPClient = {
    conn.login(username, password);
    return this
  }

  def sendText(username: String, message: String): XMPPClient = {
    val m = new Message(username)
    m.addBody("en_US", message)
    conn.sendPacket(m)
    return this
  }

}

object XMPPClient {

  private def getConfig(server: String, port: Int): ConnectionConfiguration = {
    val config: ConnectionConfiguration = new ConnectionConfiguration(server, port);
    config.setSASLAuthenticationEnabled(true);
    config.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled)
    return config
  }
  def main(args: Array[String]) {
    new XMPPClient(args(0), Integer.valueOf(args(1)))
      .connect()
      .login(args(2), args(3))
      .sendText(args(4), args(5))
      .disconnect()
  }

}