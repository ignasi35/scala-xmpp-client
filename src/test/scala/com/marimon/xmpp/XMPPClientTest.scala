package com.marimon.xmpp
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class XMPPClientTest extends FlatSpec with ShouldMatchers {

  // First batch of tests disabled since many servers seemed 
  // to crash under strange usage (connect/disconnect, 
  // connect/login/disconnect, ... anything without 
  // actually delivering information.

  //  "An XMPPClient" should "connect and disconnect from the XMMP server" in {
  //    new XMPPClient("jabber.org", 5222).connect().disconnect()
  //  }
  //
  //  it should "be able to login" in {
  //    new XMPPClient("jabber.org", 5223).connect()
  //      .login("xmppscalaclient@jabber.org", "xmppscalaclient")
  //      .disconnect()
  //  }

  "An XMPPClient" should "be able to send text messages" in {
    new XMPPClient("jabber.org", 5222)
      .connect()
      .login("xmppscalaclient@jabber.org", "xmppscalaclient")
      .sendText("ignasi36@jabber.org", "Hello World!")
      .disconnect()
  }

}